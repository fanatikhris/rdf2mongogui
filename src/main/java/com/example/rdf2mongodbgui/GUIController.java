package com.example.rdf2mongodbgui;

import org.apache.commons.io.FileUtils;
import RDF2Mongo.RDF2Mongo;
import RDF2Mongo.MongoQuery;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import com.jfoenix.controls.JFXButton;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.jena.riot.Lang;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.jena.riot.RDFLanguages;
import java.io.File;
import java.util.Collection;
import java.util.Optional;

public class GUIController {

    private RDF2Mongo rdf2Mongo;


    @FXML
    private TextField elementIdInput;
    @FXML
    private SplitPane allContentPane;

    @FXML
    private JFXComboBox<String> collectionSelect;

    @FXML
    private Label resultText;

    @FXML
    private JFXTextArea filter;

    @FXML
    private JFXTextArea projection;
    private Stage stage;
    @FXML
    private JFXTextArea sort;

    @FXML
    private MenuItem connectBtn;
    @FXML
    private JFXButton dropBtn;
    @FXML
    private JFXComboBox<String> dbSelect;

    private String urlConnect;

    public void initialize(){
        rdf2Mongo = new RDF2Mongo();
        connectBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                TextInputDialog dialog = new TextInputDialog();
                dialog.setTitle("Connect to mongodb");
                dialog.setHeaderText("Enter mongodb url:");
                dialog.setContentText("URL:");
                dialog.getEditor().setText("mongodb://localhost:27017");
                Optional<String> result = dialog.showAndWait();
                result.ifPresent(url -> {
                    try{
                        rdf2Mongo.initConnection(url);
                        urlConnect = url;
                        dbSelect.getItems().clear();
                        for (String db : rdf2Mongo.listDbs()) {
                            dbSelect.getItems().add(db);
                        }
                        dbSelect.valueProperty().addListener(new ChangeListener<String>() {
                            @Override
                            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                                if(t1 != null && t1 != "") {
                                    try {
                                        rdf2Mongo.setDatabase(t1);
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                    collectionSelect.getItems().clear();
                                    try {
                                        for (String col : rdf2Mongo.listCollections()) {
                                            collectionSelect.getItems().add(col);
                                        }
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }

                            }
                        });



                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Test Connection");
                        alert.setHeaderText("Connected !");
                        alert.setContentText("Connect to the database successfully!");
                        allContentPane.setDisable(false);

                        alert.showAndWait();
                    } catch (Exception e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error alert");
                        alert.setHeaderText("Cannot connect to mongodb");
                        alert.setContentText(e.getMessage());
                        alert.showAndWait();

                    }
                });
            }
        });
    }

//{$and: [{label: 'Mooney Restaurant Data as an Ontology, OWL Lite'}, {"@id": 'http://www.mooney.net/restaurant' }]}
    @FXML
    private void executeQuery(ActionEvent event) {
        try
        {
            if(dbSelect.getValue() == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert");
                alert.setHeaderText("No database chosen");

                alert.showAndWait();
                return;
            }
            if(collectionSelect.getValue() == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert");
                alert.setHeaderText("No collection chosen");

                alert.showAndWait();
                return;
            }
            MongoQuery mquery = new MongoQuery(urlConnect,dbSelect.getValue(),collectionSelect.getValue());

            if (filter.getText()!=null && filter.getText().length()>0)
                mquery.setFilter(filter.getText().trim());
            if (projection.getText()!=null && projection.getText().length()>0)
                mquery.setProjection(projection.getText().trim());
            if (sort.getText()!=null && sort.getText().length()>0)
                mquery.setSort(sort.getText().trim());
            String res = mquery.makeQuery();
            JSONArray jsonObject = new JSONArray(res);
            resultText.setText(jsonObject.toString(4));
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    }

    public void setStage(Stage st){
        this.stage = st;
    }

    public void dropDB(ActionEvent actionEvent) {
        if(dbSelect.getValue() != ""){
            try {
                rdf2Mongo.deleteDatabase();
                dbSelect.getItems().clear();
                collectionSelect.getItems().clear();
                for (String db : rdf2Mongo.listDbs()) {
                    dbSelect.getItems().add(db);
                }
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Drop database");
                alert.setHeaderText("Database Dropped");
                allContentPane.setDisable(false);

                alert.showAndWait();
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert");
                alert.setHeaderText("An exception has occurred");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
                throw new RuntimeException(e);
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error alert");
            alert.setHeaderText("No database chosen");

            alert.showAndWait();
        }
    }

    public void createDB(ActionEvent actionEvent) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Create database");
        dialog.setHeaderText("Enter database name:");
        dialog.setContentText("DBNAME:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(dbName -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Ontology");
            alert.setContentText("Do you want to add an ontology ?");
            ButtonType okButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
            ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
            ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(okButton, noButton, cancelButton);
            alert.showAndWait().ifPresent(type -> {

                if (type.getButtonData() == ButtonBar.ButtonData.YES) {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Select OWL ontology");
                    File f = fileChooser.showOpenDialog(stage);
                    try {
                        String res = rdf2Mongo.initDatabase(dbName, FileUtils.readFileToString(f, "utf8"), RDFLanguages.RDFXML);
                        dbSelect.getItems().add(dbName);
                        dbSelect.setValue(dbName);
                        JSONObject resJson = new JSONObject(res);
                        Alert alertInfo = new Alert(Alert.AlertType.INFORMATION);
                        alertInfo.setTitle("DB Creation");
                        alertInfo.setHeaderText("Database Created !");
                        alertInfo.setContentText(resJson.getInt("OntologySize") + " triples imported from ontology !");


                        alertInfo.showAndWait();

                    } catch (Exception e) {
                        Alert alertError = new Alert(Alert.AlertType.ERROR);
                        alertError.setTitle("Error alert");
                        alertError.setHeaderText("An exception has occurred");
                        alertError.setContentText(e.getMessage());
                        alertError.showAndWait();
                    }
                } else if (type.getButtonData() == ButtonBar.ButtonData.NO) {
                    try {
                        rdf2Mongo.initDatabase(dbName);
                        dbSelect.getItems().add(dbName);
                        dbSelect.setValue(dbName);
                        Alert alertInfo = new Alert(Alert.AlertType.INFORMATION);
                        alertInfo.setTitle("DB Creation");
                        alertInfo.setHeaderText("Database Created !");


                        alertInfo.showAndWait();
                    } catch (Exception e) {
                        Alert alertError = new Alert(Alert.AlertType.ERROR);
                        alertError.setTitle("Error alert");
                        alertError.setHeaderText("An exception has occurred");
                        alertError.setContentText(e.getMessage());
                        alertError.showAndWait();
                    }
                } else {
                }
            });
        });
    }


    public void deleteElement(ActionEvent actionEvent) {
        if(dbSelect.getValue() != null && elementIdInput.getText() != null){
            if(!collectionSelect.getItems().contains("@data")){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert");
                alert.setHeaderText("An exception has occurred");
                alert.setContentText("Database doesn't contain @data collection");
                alert.showAndWait();
                return;
            }
            try {
                String res = rdf2Mongo.deleteElement(elementIdInput.getText());
                JSONObject jsonObject = new JSONObject(res);
                String response = "1 element Deleted from @data";
                if(jsonObject.has("inferencesDeleted")){
                    response += "\n" + jsonObject.getInt("inferencesDeleted") + " element(s) deleted from @results";
                }
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Delete Element");
                alert.setHeaderText("Element Deleted");
                alert.setContentText(response);


                alert.showAndWait();
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error alert");
                alert.setHeaderText("An exception has occurred");
                alert.setContentText(e.getMessage());
                alert.showAndWait();
                throw new RuntimeException(e);
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error alert");
            alert.setHeaderText("You need to choose db and valid element id");

            alert.showAndWait();
        }
    }

    public void importRDF(ActionEvent actionEvent) {
        if(dbSelect.getValue() != null && collectionSelect.getItems().contains("@data")) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select RDF Data");
            File f = fileChooser.showOpenDialog(stage);
            ChoiceDialog<Lang> dialog = new ChoiceDialog<Lang>();
            dialog.setTitle("RDF Type");
            dialog.setHeaderText("Select a type:");
            dialog.setContentText("type:");
            dialog.getItems().setAll(RDFLanguages.getRegisteredLanguages());
            Optional<Lang> result = dialog.showAndWait();

            try {
                String res = rdf2Mongo.importRDF(FileUtils.readFileToString(f, "utf8"), result.get());
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Import RDF");
                alert.setHeaderText("Data Imported !");

                JSONObject jsonObject = new JSONObject(res);
                String response = jsonObject.getInt("dataImported") + " triple(s) added to @data";
                if(jsonObject.has("dataInferred")){
                    response += "\n" + jsonObject.getInt("dataInferred") + " triple(s) added to @results";
                }

                alert.setContentText(response);

                alert.showAndWait();

            } catch (Exception e) {
                Alert alertError = new Alert(Alert.AlertType.ERROR);
                alertError.setTitle("Error alert");
                alertError.setHeaderText("An exception has occurred");
                alertError.setContentText(e.getMessage());
                alertError.showAndWait();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error alert");
            alert.setHeaderText("You need to choose db with @data collection");

            alert.showAndWait();
        }
    }

    public void getCollectionStats(ActionEvent actionEvent) {
        if(collectionSelect.getValue() == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error alert");
            alert.setHeaderText("You need to choose a collection");

            alert.showAndWait();
            return;
        }

        try {
            String res = rdf2Mongo.getCollectionStats(collectionSelect.getValue());
            JSONObject jsonObject = new JSONObject(res);
            jsonObject.remove("indexDetails");
            jsonObject.remove("wiredTiger");
            resultText.setText(jsonObject.toString(4));
        } catch (Exception e) {
            Alert alertError = new Alert(Alert.AlertType.ERROR);
            alertError.setTitle("Error alert");
            alertError.setHeaderText("An exception has occurred");
            alertError.setContentText(e.getMessage());
            alertError.showAndWait();
        }
    }
}
