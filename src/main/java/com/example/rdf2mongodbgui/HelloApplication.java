package com.example.rdf2mongodbgui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("GUI.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1000, 600);
        stage.setTitle("rdf2mongo");
        stage.setScene(scene);
        stage.setResizable(false);
        GUIController guiController = (GUIController)fxmlLoader.getController();
        guiController.setStage(stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}