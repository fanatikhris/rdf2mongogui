module com.example.rdf2mongodbgui {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.jfoenix;

    requires json;
    requires rdftomongodb;
    requires java.net.http;
    requires org.apache.commons.io;
    requires org.apache.jena.arq;


    opens com.example.rdf2mongodbgui to javafx.fxml;
    exports com.example.rdf2mongodbgui;
}